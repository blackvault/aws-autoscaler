from setuptools import setup, find_packages

setup(
    name="autoscaler",
    packages=find_packages("src"),
    package_dir={"": "src"},
    install_requires=[
        "boto3"
    ],
    extras_require={
        "dev": [
            "pytest",
        ],
    },
)