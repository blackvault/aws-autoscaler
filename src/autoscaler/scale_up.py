import boto3


def scale_up(client, values):
    """Sets autoscaling groups to old values"""
    for value in values:
        response = client.update_auto_scaling_group(
            AutoScalingGroupName = value['instance_name'],
            DesiredCapacity = int(value['desired_state_capacity']),
            MinSize = int(value['minimum_size'])
        )


def read_values(dynamodb):
    """Return values from dynamodb"""
    table_name = 'autoscaling_groups'
    table = dynamodb.Table(table_name)
    response = table.scan()
    data = response['Items']
    return data


def main():
    dynamo_db_resource = boto3.resource('dynamodb')
    asg_client = boto3.client('autoscaling')
    values = read_values(dynamo_db_resource)
    scale_up(asg_client, values)


if __name__ == '__main__':
    main()