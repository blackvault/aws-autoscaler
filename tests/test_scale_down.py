from autoscaler.scale_down import list_autoscaling_groups
import boto3

def test_list_autoscaling_groups():
    client = boto3.client('autoscaling')
    d = list_autoscaling_groups(client)
    assert d
    assert len(d) > 0

    for k, v in d.items():
        assert isinstance(k, str)
        assert isinstance(v['desired_capacity'], int)
        assert isinstance(v['minimum_size'], int)