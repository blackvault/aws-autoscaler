from autoscaler.scale_up import read_values
import boto3

def test_read_values():
    client = boto3.resource('dynamodb')
    d = read_values(client)
    assert d
    assert len(d) > 0
    for i in d:
        instance_name = i['instance_name']
        desired_state = int(i['desired_state_capacity'])
        minimum_size = int(i['minimum_size'])
        assert isinstance(instance_name, str)
        assert isinstance(desired_state, int)
        assert isinstance(minimum_size, int)