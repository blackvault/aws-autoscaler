resource "aws_iam_role_policy" "scale_down_policy" {
  name   = "scale-down-policy"
  role = "${aws_iam_role.scale_down_role.id}"
  policy = "${data.aws_iam_policy_document.scale_down_policy.json}"
}
data "aws_iam_policy_document" "scale_down_policy" {
  statement {
    sid = ""
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:UpdateAutoScalingGroup"
    ]
    effect = "Allow"
    resources = [
      "*"
    ]
  }
  statement {
    sid = ""
    actions = [
      "dynamodb:PutItem"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:${var.region}:*:table/${var.dynamodb_table_name}"
    ]
  }
  statement {
    sid = ""
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:TagLogGroup"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}
resource "aws_iam_role" "scale_down_role" {
  name = "scale_down_role"
  assume_role_policy = "${data.aws_iam_policy_document.scale_down_assume_role_policy.json}"
}
data "aws_iam_policy_document" "scale_down_assume_role_policy" {

    statement {
      effect = "Allow"
      actions = [
        "sts:AssumeRole",
      ]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com"
      ]
    }
      sid = ""
    }
}