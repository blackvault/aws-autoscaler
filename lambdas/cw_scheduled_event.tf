resource "aws_cloudwatch_event_rule" "scale_down_rule" {
    name                = "scale_down_rule"
    description         = "schedule events for scale_down"
    schedule_expression = "${var.scale_down_time}"
}

resource "aws_cloudwatch_event_target" "lambda_schedular_scale_down" {
    rule =  "${aws_cloudwatch_event_rule.scale_down_rule.name}"
    arn  =   "${aws_lambda_function.scale_down.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_scale_down" {
   statement_id = "AllowExecutionFromCloudWatch"
   action = "lambda:InvokeFunction"
   function_name = "${aws_lambda_function.scale_down.function_name}"
   principal = "events.amazonaws.com"
   source_arn = "${aws_cloudwatch_event_rule.scale_down_rule.arn}"
}
resource "aws_cloudwatch_event_rule" "scale_down_weekend" {
    name                = "scale_down_weekend"
    description         = "schedule events for scale_down"
    schedule_expression = "${var.scale_down_weekend_time}"
}

resource "aws_cloudwatch_event_target" "lambda_schedular_scale_down_weekend" {
    rule =  "${aws_cloudwatch_event_rule.scale_down_weekend.name}"
    arn  =   "${aws_lambda_function.scale_down.arn}"
}

resource "aws_cloudwatch_event_rule" "scale_up_rule" {
    name                = "scale_up_rule"
    description         = "schedule events for scale up"
    schedule_expression = "${var.scale_up_time}"
}

resource "aws_cloudwatch_event_target" "lambda_schedular_scale_up" {
    rule =  "${aws_cloudwatch_event_rule.scale_up_rule.name}"
    arn  =   "${aws_lambda_function.scale_up.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_scale_up" {
   statement_id = "AllowExecutionFromCloudWatch"
   action = "lambda:InvokeFunction"
   function_name = "${aws_lambda_function.scale_up.function_name}"
   principal = "events.amazonaws.com"
   source_arn = "${aws_cloudwatch_event_rule.scale_up_rule.arn}"
}

resource "aws_cloudwatch_event_rule" "scale_up_weekend" {
    name                = "scale_up_weekend"
    description         = "schedule events for scale up"
    schedule_expression = "${var.scale_up_weekend_time}"
}

resource "aws_cloudwatch_event_target" "lambda_schedular_scale_up_weekend" {
    rule =  "${aws_cloudwatch_event_rule.scale_up_weekend.name}"
    arn  =   "${aws_lambda_function.scale_up.arn}"
}



