resource "aws_lambda_function" "scale_up" {
  filename      = "${var.scale_up_output}"
  function_name = "scale_up"
  role          = "${aws_iam_role.scale_up_role.arn}"
  handler       = "scale_up.handler"
  runtime = "python3.7"
  source_code_hash = "${base64sha256(file("${data.archive_file.scale_up.output_path}"))}"
}
data "archive_file" "scale_up" {
  type        = "zip"
  source_file = "${var.scale_up_source}"
  output_path = "${var.scale_up_output}"
}