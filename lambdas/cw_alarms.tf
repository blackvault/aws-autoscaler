resource "aws_cloudwatch_metric_alarm" "scale-up-errors" {
  alarm_name          = "autoscaler-scale-up-errors"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"
  period              = "60"
  statistic           = "Maximum"
  threshold           = 0
  alarm_description   = "Scale up Execution Errors"
  treat_missing_data  = "ignore"

  insufficient_data_actions = [
    "${aws_sns_topic.alarms.arn}",
  ]

  alarm_actions = [
    "${aws_sns_topic.alarms.arn}",
  ]

  ok_actions = [
    "${aws_sns_topic.alarms.arn}",
  ]

  dimensions {
    FunctionName = "${aws_lambda_function.scale_up.function_name}"
    Resource     = "${aws_lambda_function.scale_up.function_name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "scale-down-errors" {
  alarm_name          = "autoscaler-scale-down-errors"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"
  period              = "60"
  statistic           = "Maximum"
  threshold           = 0
  alarm_description   = "Scale down Execution Errors"
  treat_missing_data  = "ignore"

  insufficient_data_actions = [
    "${aws_sns_topic.alarms.arn}",
  ]

  alarm_actions = [
    "${aws_sns_topic.alarms.arn}",
  ]

  ok_actions = [
    "${aws_sns_topic.alarms.arn}",
  ]

  dimensions {
    FunctionName = "${aws_lambda_function.scale_down.function_name}"
    Resource     = "${aws_lambda_function.scale_down.function_name}"
  }
}