resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "${var.dynamodb_table_name}"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "instance_name"
  range_key      = "desired_state_capacity"

  attribute {
    name = "instance_name"
    type = "S"
  }

  attribute {
    name = "desired_state_capacity"
    type = "N"
  }

  tags = {
    Name        = "${var.dynamodb_tags_name}"
    Environment = "${var.dynamodb_tags_env}"
  }
}
