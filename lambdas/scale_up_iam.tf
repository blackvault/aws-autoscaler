resource "aws_iam_role_policy" "scale_up_policy" {
  name = "scale_up_policy"
  role = "${aws_iam_role.scale_up_role.id}"
  policy ="${data.aws_iam_policy_document.scale_up_policy.json}"
}
data "aws_iam_policy_document" "scale_up_policy" {
  statement {
    sid = ""
    actions = [
      "dynamodb:Scan"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:${var.region}:*:table/${var.dynamodb_table_name}"
    ]
  }
    statement {
    sid = ""
    actions = [
      "autoscaling:UpdateAutoScalingGroup"
    ]
    effect = "Allow"
    resources = [
      "*"
    ]
  }
    statement {
    sid = ""
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:TagLogGroup"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}
resource "aws_iam_role" "scale_up_role" {
  name = "scale_up_role"
  assume_role_policy = "${data.aws_iam_policy_document.scale_up_assume_role_policy.json}"
}
data "aws_iam_policy_document" "scale_up_assume_role_policy" {
   statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}