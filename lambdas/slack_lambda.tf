resource "aws_lambda_function" "slack" {
  filename         = "${data.archive_file.slack.output_path}"
  function_name    = "slack"
  role             = "${aws_iam_role.autoscaler.arn}"
  handler          = "slack.handler"
  runtime          = "python3.7"
  environment {
    variables = {
      SLACK_WEBHOOK_URL = "${var.slack_webhook}"
    }
  }
  source_code_hash = "${base64sha256(file("${data.archive_file.slack.output_path}"))}"
}
data "archive_file" "slack" {
  type        = "zip"
  source_file = "${var.slack_source}"
  output_path = "${var.slack_output}"
}
resource "aws_lambda_permission" "slack" {
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.slack.function_name}"
  principal     = "sns.amazonaws.com"
  source_arn    = "${aws_sns_topic.alarms.arn}"
}
resource "aws_iam_role" "autoscaler" {
  name = "iam-role"
  assume_role_policy = "${data.aws_iam_policy_document.lambda.json}"
}
data "aws_iam_policy_document" "lambda" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}
