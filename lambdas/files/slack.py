import json
import time
import os
import logging
import urllib.request
from urllib.error import URLError, HTTPError

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):

    try:
        send_slack_message(event)
        logger.info(event)
        return

    except Exception as e:
        logger.error(e)


def send_slack_message(event, retry=3):

    message = json.loads(event["Records"][0]["Sns"]["Message"])
    resource = message["Trigger"]["Namespace"]
    metric = message["Trigger"]["MetricName"]
    status = message["NewStateValue"]
    detail = message["NewStateReason"]

    message = {
        "icon_emoji": ":ok:",
        "username" : "autoscaler",
        "attachments" : [
            {
                "title" : "- CloudWatch Alarm",
                "fields" : [
                    {
                        "title" : "Resource",
                        "value" : resource,
                        "short" : True
                    },
                    {
                        "title" : "Metric",
                        "value" : metric,
                        "short" : True
                    },
                    {
                        "title" : "Status",
                        "value" : status,
                        "short" : True
                    },
                    {
                        "title" : "Detail",
                        "value" : message['AlarmName'],
                        "short" : True
                    }
                ]
            }
        ]
    }

    if status == "OK":
        message["attachments"][0]["color"] = "#7CD197"
    else:
        message["attachments"][0]["color"] = "F35A00"
        message["icon_emoji"] = ":angry_dog:"

    data = json.dumps(message).encode("utf-8")
    webhook_url = os.environ['SLACK_WEBHOOK_URL']

    for _ in range(retry):
        res_code = send_request(webhook_url, data)
        if res_code == 200:
            return
        time.sleep(1)


def send_request(url, data):

    try:
        req = urllib.request.Request(
            url=url,
            data=data,
            method="POST"
        )
        res = urllib.request.urlopen(req)
        return res.code

    except HTTPError as e:
        logging.error(e)
        return e.code

    except URLError as e:
        logging.error(e)
        return