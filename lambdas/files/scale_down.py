import boto3


def scale_down(client, names):
    """Terminate instances"""
    for name in names:
        response = client.update_auto_scaling_group(
            AutoScalingGroupName=name,
            DesiredCapacity=0,
            MinSize=0
        )


def populate_db(dynamo_db, data):
    """Add values to db"""
    table = dynamo_db.Table('autoscaling_groups')
    for name, value in data.items():
        table.put_item(
        Item={
                'instance_name':name,
                'desired_state_capacity':value['desired_capacity'],
                'minimum_size':value['minimum_size']
            }
        )


def list_autoscaling_groups(client):
    """Get autoscaling group values"""
    instances = client.describe_auto_scaling_groups()
    instance_name_capacity = {}
    for i in instances['AutoScalingGroups']:
        instance_name_capacity.update({i['AutoScalingGroupName']:{
            "desired_capacity" : i['DesiredCapacity'],
            "minimum_size" : i['MinSize']
        }})
    return instance_name_capacity


def handler(event, context):
    dynamo_db_resource = boto3.resource('dynamodb')
    asg_client = boto3.client('autoscaling')
    instance_name_capacity = list_autoscaling_groups(asg_client)
    populate_db(dynamo_db_resource, instance_name_capacity)
    scale_down(asg_client, instance_name_capacity)



