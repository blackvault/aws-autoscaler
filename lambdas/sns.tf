resource "aws_sns_topic" "alarms" {
  name = "${var.sns_topic_name}"
}
resource "aws_sns_topic_subscription" "subscription" {
  topic_arn = "${aws_sns_topic.alarms.arn}"
  protocol  = "lambda"
  endpoint  = "${aws_lambda_function.slack.arn}"
}