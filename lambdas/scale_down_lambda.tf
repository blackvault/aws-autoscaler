resource "aws_lambda_function" "scale_down" {
  filename      = "${var.scale_down_output}"
  function_name = "scale_down"
  role          = "${aws_iam_role.scale_down_role.arn}"
  handler       = "scale_down.handler"
  runtime = "python3.7"
  source_code_hash = "${base64sha256(file("${data.archive_file.scale_down.output_path}"))}"
}
data "archive_file" "scale_down" {
  type        = "zip"
  source_file = "${var.scale_down_source}"
  output_path = "${var.scale_down_output}"
}