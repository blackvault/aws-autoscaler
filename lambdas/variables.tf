variable "region" {
    default = "eu-central-1"
}

variable "slack_webhook" {
    default = "<webhook>"
}

variable "slack_source" {
  default = "files/slack.py"
}

variable "slack_output" {
  default = "files/slack.zip"
}

variable "scale_down_source" {
  default = "files/scale_down.py"
}

variable "scale_down_output" {
  default = "files/scale_down.zip"
}

variable "scale_up_source" {
  default = "files/scale_up.py"
}

variable "scale_up_output" {
  default = "files/scale_up.zip"
}

variable "dynamodb_table_name" {
    default = "autoscaling_groups"
}

variable "dynamodb_tags_name" {
  default = "autoscaling-groups-desired-state"
}

variable "dynamodb_tags_env" {
  default = "prod"
}

variable "scale_down_time" {
  default = "cron(0 19 ? * MON-FRI *)"
}

variable "scale_down_weekend_time" {
  default = "cron(0 7 ? * SAT-SUN *)"
}

variable "scale_up_time" {
  default = "cron(30 4 ? * MON-FRI *)"

}

variable "scale_up_weekend_time" {
  default = "cron(30 4 ? * SAT-SUN *)"

}

variable "sns_topic_name" {
  default = "autoscaler-alarms"
}
